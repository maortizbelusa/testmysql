-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dm_blank_price_normal`
--

DROP TABLE IF EXISTS `dm_blank_price_normal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dm_blank_price_normal` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(30) NOT NULL,
  `product_color` varchar(50) NOT NULL,
  `price_12_35` double NOT NULL DEFAULT '0',
  `price_36_71` double NOT NULL DEFAULT '0',
  `price_72_143` double NOT NULL DEFAULT '0',
  `price_144_287` double NOT NULL DEFAULT '0',
  `price_288_575` double NOT NULL DEFAULT '0',
  `price_576_1007` double NOT NULL DEFAULT '0',
  `price_1008_2015` double NOT NULL DEFAULT '0',
  `price_2016` double NOT NULL DEFAULT '0',
  `price_7500_9999` double NOT NULL DEFAULT '0',
  `price_10000` double NOT NULL DEFAULT '0',
  `price_20000` double NOT NULL DEFAULT '0',
  `price_30000` double NOT NULL DEFAULT '0',
  `price_slab_13` double NOT NULL,
  `price_slab_14` double NOT NULL,
  `price_slab_15` double NOT NULL,
  `price_slab_16` double NOT NULL,
  `price_slab_17` double NOT NULL,
  `price_slab_18` double NOT NULL,
  `price_slab_19` double NOT NULL,
  `price_slab_20` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_code_idx` (`product_code`)
) ENGINE=InnoDB AUTO_INCREMENT=59276 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:20:49
