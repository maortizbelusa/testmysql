-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'dbpromo'
--
/*!50003 DROP PROCEDURE IF EXISTS `DLGetCategoryColorList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DLGetCategoryColorList`(ItemCode varchar(50))
BEGIN


IF (SELECT 
    Count(*)
FROM
    dbpromo.IMPRINT_COLOR_CHART ICC
        INNER JOIN
    dbpromo.IMPRINT_COLORS_LIST ICL ON ICC.COLOR_ID = ICL.COL_ID

        INNER JOIN
    dbpromo.ITEM_MASTER IM ON IM.CAT_ID = ICC.CAT_ID
WHERE
    ITEM_CODE = ItemCode
        AND COL_NAME NOT LIKE 'Custom%'
 ) > 0 
THEN 
SELECT 
    ICC.*, ICL.*
FROM
    dbpromo.IMPRINT_COLOR_CHART ICC
        INNER JOIN
    dbpromo.IMPRINT_COLORS_LIST ICL ON ICC.COLOR_ID = ICL.COL_ID

        INNER JOIN
    dbpromo.ITEM_MASTER IM ON IM.CAT_ID = ICC.CAT_ID
WHERE
    ITEM_CODE = ItemCode
        AND COL_NAME NOT LIKE 'Custom%'
ORDER BY ICC.DISP_SEQ;
ELSE
	SELECT * 
	FROM IMPRINT_COLOR_CHART ICC, IMPRINT_COLORS_LIST ICL 
	WHERE ICC.COLOR_ID=ICL.COL_ID AND ICC.CAT_ID='15' AND COL_NAME not like 'Custom%'   
	ORDER BY ICC.DISP_SEQ;
 END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_DeleteAssetByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_DeleteAssetByCustomer`(AssetID INT)
BEGIN

UPDATE DL_CUSTOMER_ASSET SET
  Visible = 0
WHERE CustomerAssetID = AssetID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_DeleteDesignByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_DeleteDesignByCustomer`(DesignID INT)
BEGIN

UPDATE dm_saved_design_template SET
  Visible = 0
WHERE design_id = DesignID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetApparelSizes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_GetApparelSizes`(ItemCode varchar(50))
BEGIN
  SELECT AVAILABLE_SIZE 
  FROM dbpromo.AVAILABLE_TSHIRT_SIZE
  WHERE ITEM_CODE = ItemCode;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetAssetByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_GetAssetByCustomer`(CustID INT)
BEGIN
	select CustomerAssetID as id, AssetData as src, AssetUuID as uuid, CompleteWhite as white
	from DL_CUSTOMER_ASSET inner join 
         DL_CUSTOMER_ASSET_TYPE on CustomerAssetTypeID = AssetTypeID 
	where CustomerID = CustID and visible = 1
	order by CreatedDateTime desc
	limit 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryColorList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryColorList`(ItemCode varchar(50))
BEGIN


IF (SELECT 
    Count(*)
FROM
    dbpromo.IMPRINT_COLOR_CHART ICC
        INNER JOIN
    dbpromo.IMPRINT_COLORS_LIST ICL ON ICC.COLOR_ID = ICL.COL_ID

        INNER JOIN
    dbpromo.ITEM_MASTER IM ON IM.CAT_ID = ICC.CAT_ID
WHERE
    ITEM_CODE = ItemCode
        AND COL_NAME NOT LIKE 'Custom%'
 ) > 0 
THEN 
SELECT 
    ICC.*, ICL.*
FROM
    dbpromo.IMPRINT_COLOR_CHART ICC
        INNER JOIN
    dbpromo.IMPRINT_COLORS_LIST ICL ON ICC.COLOR_ID = ICL.COL_ID

        INNER JOIN
    dbpromo.ITEM_MASTER IM ON IM.CAT_ID = ICC.CAT_ID
WHERE
    ITEM_CODE = ItemCode
        AND COL_NAME NOT LIKE 'Custom%'
ORDER BY ICC.DISP_SEQ;
ELSE
	SELECT * 
	FROM IMPRINT_COLOR_CHART ICC, IMPRINT_COLORS_LIST ICL 
	WHERE ICC.COLOR_ID=ICL.COL_ID AND ICC.CAT_ID='15' AND COL_NAME not like 'Custom%'   
	ORDER BY ICC.DISP_SEQ;
 END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryLayoutDesignByID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryLayoutDesignByID`(ID INT)
BEGIN

select CM.CAT_TITLE , Sequence, Enable, CatLayoutDesignName, Design_Info,Preview_URL
from 
dbpromo.DL_CAT_LAYOUT_DESIGN LD inner join 
dbpromo.DL_LAYOUT_DESIGN D on D.LayoutDesignID = LD.LayoutDesignID inner join
dbpromo.CATEGORY_MASTER CM on CM.CAT_ID = LD.Cat_ID
 WHERE LD.CatLayoutDesignID = ID 
 ORDER BY Sequence;
 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryLayoutDesignByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryLayoutDesignByProduct`(ItemCode NVARCHAR(50))
BEGIN

IF (select allow_layout from ITEM_MASTER im where im.item_code = ItemCode) = 1
THEN
SELECT Sequence, Enable, CatLayoutDesignName, Design_Info,Preview_URL, TextOption, IconOption, ImageOption
FROM dbpromo.ITEM_MASTER IM inner join
     dbpromo.DL_CAT_LAYOUT_DESIGN LD on IM.CAT_ID = LD.Cat_ID inner join 
     dbpromo.DL_LAYOUT_DESIGN D on D.LayoutDesignID = LD.LayoutDesignID
 WHERE IM.item_code = ItemCode and Enable = 1
 ORDER BY Sequence;
 END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryLayoutOptionsByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryLayoutOptionsByProduct`(ItemCode varchar(50))
BEGIN
IF (SELECT count(*)
FROM dbpromo.ITEM_MASTER IM inner join
     dbpromo.DL_CAT_LAYOUT_OPTIONS CO on IM.CAT_ID = CO.Cat_ID 
 WHERE IM.item_code = ItemCode) > 0
THEN
SELECT CO.Logo_URL_OneColor,CO.Logo_URL_Color,CO.Text,CO.Clip_URL
FROM dbpromo.ITEM_MASTER IM inner join
     dbpromo.DL_CAT_LAYOUT_OPTIONS CO on IM.CAT_ID = CO.Cat_ID
 WHERE IM.item_code = ItemCode; 
 ELSE 
 SELECT Logo_URL_OneColor,Logo_URL_Color,Text,Clip_URL
 FROM dbpromo.DL_DEFAULT_LAYOUT_OPTIONS CO;
 END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryOptionsByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryOptionsByProduct`(ItemCode varchar(50))
BEGIN
IF (SELECT count(*)
  FROM dbpromo.ITEM_MASTER IM inner join
  dbpromo.DL_CAT_CONFIG_SETTING CS  on IM.CAT_ID = CS.CategoryID inner join 
  dbpromo.DL_CONFIG_MASTER CM on CS.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE IM.item_code = ItemCode  ) > 0
  THEN
 SELECT OB.ObjectName, OT.OptionName, CD.OptionValue  
  FROM dbpromo.ITEM_MASTER IM inner join
  dbpromo.DL_CAT_CONFIG_SETTING CS  on IM.CAT_ID = CS.CategoryID inner join 
  dbpromo.DL_CONFIG_MASTER CM on CS.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE IM.item_code = ItemCode;
  ELSE
   SELECT OB.ObjectName, OT.OptionName, CD.OptionValue  
  FROM
  dbpromo.DL_CONFIG_MASTER CM inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE CM.ConfigName = 'Default'; 
  end if;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetCategoryTitleByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetCategoryTitleByProduct`(ItemCode varchar(50))
BEGIN

IF (SELECT count(*)
  FROM dbpromo.ITEM_MASTER IM 
  WHERE IM.item_code = ItemCode  ) > 0
THEN
	SELECT DISTINCT CASE WHEN CF.CAT_TITLE in ('glassware','clothing accessories','Ceramic Mugs','Tote Bags','Frisbees','Keychains','Notebooks','Pens','T Shirts','Water Bottles','Stress Balls','Travel Mugs','Calculator','Golf','Office','Wellness Safety Products','Mouse Pads','USB Flash Drives','Tools','Dinnerware','Outdoor and Leisure','Auto Home Tools') 
                    THEN LOWER(replace(CF.CAT_TITLE,' ', '_')) 
                    ELSE LOWER(replace('Ceramic Mugs',' ', '_')) END as CAT_TITLE
	FROM  dbpromo.ITEM_MASTER IM inner join
	      dbpromo.CATEGORY_MASTER CM on IM.CAT_ID = CM.CAT_ID inner join 
          dbpromo.CATEGORY_MASTER CF on CF.CAT_ID = case when CM.parent = 0 then CM.CAT_ID else CM.parent end
	WHERE IM.ITEM_CODE = ItemCode;
ELSE
  SELECT LOWER(replace('Ceramic Mugs',' ', '_')) CAT_TITLE;
  END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetDesignsByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetDesignsByCustomer`(Customer_ID int)
BEGIN
SELECT sv.design_id,o.design_info,sv.id, sv.saved_time as date
FROM dbpromo.dm_saved_design_template as sv 
inner join discm_discountmugs.dmlab_orders as o on o.id = sv.design_id 
where sv.user_id = Customer_ID 
AND YEAR(sv.saved_time) >= '2019' 
AND sv.lab_type = '3d' 
AND visible = 1
order by sv.saved_time desc 
limit 20;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetImprintSizesByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetImprintSizesByProduct`(ItemCode NVARCHAR(50))
BEGIN

SELECT  I.Item_Code, Print_Option, Location, ImprintHeight, ImprintWidth
FROM dbpromo.DL_IMPRINT_SIZES I
WHERE I.Item_Code = ItemCode
ORDER BY Print_Option, Location;

SELECT * FROM PRODUCT_PRINT_OPTIONS po where po.Item_Code = ItemCode;

SELECT * FROM PRODUCT_IMPRINT_AREA ia WHERE ia.ITEM_CODE = ItemCode;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetIsCalendarbyProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_GetIsCalendarbyProduct`(ItemCode varchar(50))
BEGIN
   IF (SELECT  CAT_TITLE FROM  dbpromo.ITEM_MASTER IM 
						 INNER JOIN  dbpromo.CATEGORY_MASTER CM 
                         ON IM.CAT_ID = CM.CAT_ID 
	                     WHERE IM.ITEM_CODE = ItemCode) 
		LIKE '%calendar%'
    THEN SELECT 1 Iscalendar;
    ELSE SELECT 0 Iscalendar;
    END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetPmsHexColorValue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_GetPmsHexColorValue`(PantoneColor varchar(50))
BEGIN
  SELECT HexValue 
  FROM CMN_Colors 
  WHERE ColorName = PantoneColor
  LIMIT 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_GetZoomFactorByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `DL_GetZoomFactorByProduct`(ItemCode VARCHAR(50))
BEGIN

SELECT 
    CASE
        WHEN
            im.ITEM_LAB_ZOOM_FACTOR IS NOT NULL
                and im.ITEM_LAB_ZOOM_FACTOR <> 0
        THEN
            im.ITEM_LAB_ZOOM_FACTOR
        ELSE IFNULL(cm.CAT_ZOOM_FACTOR, 0)
    END ZOOM_FACTOR
FROM
    dbpromo.ITEM_MASTER AS im
        INNER JOIN
    dbpromo.BEL_CATEGORY_MASTER AS cm ON cm.cat_id = im.cat_id
WHERE im.item_code = ItemCode;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_SaveCustomerAsset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_SaveCustomerAsset`(Asset varchar(500), CustID int, Assetype int, AssetUu  varchar(250), IsWhite bit(1))
BEGIN

	INSERT INTO dbpromo.DL_CUSTOMER_ASSET
		(AssetData, 
		CustomerID,
		AssetTypeID,
		AssetUuID,
        CompleteWhite)
	VALUES
		(Asset,
		CustID, 
		Assetype, 
		AssetUu,
        IsWhite);
        
    SELECT LAST_INSERT_ID() AS ID;        
        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DL_SaveCustomerDesignData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `DL_SaveCustomerDesignData`(ProdCode varchar(50), ProdColor varchar(50), DesignInfo mediumtext, FrontImage varchar(255), BackImage varchar(255), DesignID int, 
                                            DesignImage varchar(200), SaveDesignID int, CustID int, Email varchar(255), LabType varchar(5), LabTypeDesign varchar(5), Act varchar(56))
BEGIN
IF EXISTS  (select * from discm_discountmugs.dmlab_orders where id = SaveDesignID ) then
            update discm_discountmugs.dmlab_orders SET 
				product_id=ProdCode,
				product_color=ProdColor,
				design_info=DesignInfo,
				front_image=FrontImage,
				back_image=BackImage,
				design_id=DesignID,
				design_img=DesignImage 
            where id=SaveDesignID;
            
			UPDATE dm_saved_design_template 
			SET 
				user_id = CustID,
				user_email = Email,
				lab_type = LabType,
				action = Act
			WHERE
				design_id = SaveDesignID;
           
			SELECT SaveDesignID AS Orderid;            

ELSE
     			 INSERT INTO discm_discountmugs.dmlab_orders
					(
					product_id,
					product_color,
					design_info,
					front_image,
					back_image,
					design_id,
					lab_type,
					design_img
					)
			VALUES (
                    ProdCode,
					ProdColor,
					DesignInfo,
					FrontImage,
					BackImage,
					DesignID,
					LabTypeDesign,
					DesignImage);  
             
	            SET @OrderID := LAST_INSERT_ID();
				SELECT @OrderID AS OrderID;  

				INSERT INTO dm_saved_design_template
					 (user_id,
					 design_id,
					 user_email,
					 lab_type,
					 action)
				 VALUES
					 (CustID,
					 @OrderID,
					 Email,
					 LabType,
					 Act);
 END IF;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetCategoryLayoutDesignByID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetCategoryLayoutDesignByID`(ID INT)
BEGIN

SELECT  CM.CAT_TITLE , Sequence, Enable, CatLayoutDesignName, Design_Info,Preview_URL
FROM dbpromo.DL_CAT_LAYOUT_DESIGN LD inner join 
     dbpromo.DL_LAYOUT_DESIGN D on D.LayoutDesignID = LD.LayoutDesignID inner join
     dbpromo.CATEGORY_MASTER CM on CM.CAT_ID = LD.Cat_ID
 WHERE LD.CatLayoutDesignID = ID 
 ORDER BY Sequence;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetCategoryLayoutOptionsByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetCategoryLayoutOptionsByProduct`(ItemCode nvarchar(50))
BEGIN
IF (SELECT count(*)
FROM dbpromo.ITEM_MASTER IM inner join
     dbpromo.DL_CAT_LAYOUT_OPTIONS CO on IM.CAT_ID = CO.Cat_ID 
 WHERE IM.item_code = ItemCode) > 0
THEN
SELECT CO.Logo_URL_OneColor,CO.Logo_URL_Color,CO.Text,CO.Clip_URL
FROM dbpromo.ITEM_MASTER IM inner join
     dbpromo.DL_CAT_LAYOUT_OPTIONS CO on IM.CAT_ID = CO.Cat_ID
 WHERE IM.item_code = ItemCode; 
 ELSE 
 SELECT Logo_URL_OneColor,Logo_URL_Color,Text,Clip_URL
 FROM dbpromo.DL_DEFAULT_LAYOUT_OPTIONS CO;
 END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetCategoryOptionsByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetCategoryOptionsByProduct`(ItemCode varchar(50))
BEGIN
IF (SELECT count(*)
  FROM dbpromo.ITEM_MASTER IM inner join
  dbpromo.DL_CAT_CONFIG_SETTING CS  on IM.CAT_ID = CS.CategoryID inner join 
  dbpromo.DL_CONFIG_MASTER CM on CS.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE IM.item_code = ItemCode  ) > 0
  THEN
 SELECT OB.ObjectName, OT.OptionName, CD.OptionValue  
  FROM dbpromo.ITEM_MASTER IM inner join
  dbpromo.DL_CAT_CONFIG_SETTING CS  on IM.CAT_ID = CS.CategoryID inner join 
  dbpromo.DL_CONFIG_MASTER CM on CS.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE IM.item_code = ItemCode;
  ELSE
   SELECT OB.ObjectName, OT.OptionName, CD.OptionValue  
  FROM
  dbpromo.DL_CONFIG_MASTER CM inner join
  dbpromo.DL_CONFIG_DETAIL CD on CD.ConfigMasterID = CM.ConfigID inner join
  dbpromo.DL_OPTION_TYPE OT on OT.OptionID = CD.OptionID inner join
  dbpromo.DL_OBJECT_TYPE OB on OB.ObjectID = OT.ObjectTypeID  
  WHERE CM.ConfigName = 'Default'; 
  END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetCategoryTitleByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetCategoryTitleByProduct`(ItemCode varchar(50))
BEGIN

IF (SELECT count(*)
  FROM dbpromo.ITEM_MASTER IM 
  WHERE IM.item_code = ItemCode  ) > 0
THEN
	SELECT DISTINCT CASE WHEN CF.CAT_TITLE in ('glassware','clothing accessories','Ceramic Mugs','Tote Bags','Frisbees','Keychains','Notebooks','Pens','T Shirts','Water Bottles','Stress Balls','Travel Mugs') 
                    THEN LOWER(replace(CF.CAT_TITLE,' ', '_')) 
                    ELSE LOWER(replace('Ceramic Mugs',' ', '_')) END as CAT_TITLE
	FROM  dbpromo.ITEM_MASTER IM inner join
	      dbpromo.CATEGORY_MASTER CM on IM.CAT_ID = CM.CAT_ID inner join 
          dbpromo.CATEGORY_MASTER CF on CF.CAT_ID = case when CM.parent = 0 then CM.CAT_ID else CM.parent end
	WHERE IM.ITEM_CODE = ItemCode;
ELSE
  SELECT LOWER(replace('Ceramic Mugs',' ', '_')) CAT_TITLE;
  END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetDesignsByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetDesignsByCustomer`(Customer_ID int)
BEGIN
 SELECT do.id design_id, do.design_info
FROM discm_discountmugs.dm_order_master om  inner join 
     discm_discountmugs.dm_order_details od on od.order_id = om.order_id   inner join
	 discm_discountmugs.dmlab_orders do on do.id = dmlab_design_id    
WHERE  dm_cust_id = Customer_ID
AND do.lab_type = '3d'
AND YEAR(order_date) >= '2019' 
ORDER BY order_date DESC
LIMIT 20;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetImprintSizesByProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mssqlconnect2`@`10.1.%` PROCEDURE `GetImprintSizesByProduct`(ItemCode NVARCHAR(50))
BEGIN

SELECT  I.Item_Code, Print_Option, Location, ImprintHeight, ImprintWidth
FROM dbpromo.DL_IMPRINT_SIZES I
WHERE I.Item_Code = ItemCode
ORDER BY Print_Option, Location;

SELECT * FROM PRODUCT_PRINT_OPTIONS po where po.Item_Code = ItemCode;

SELECT * FROM PRODUCT_IMPRINT_AREA ia WHERE ia.ITEM_CODE = ItemCode;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetItemRelatedSeller` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `GetItemRelatedSeller`(ItemCode varchar(50))
BEGIN

SELECT 
    ITEM_RELATION_TOPSELLER.ITEM_CODE_FOR ITEM_CODE
FROM
    ITEM_RELATION_TOPSELLER
        INNER JOIN
    (SELECT 
        item_code, cat_ID_FOR, MIN(data_source)  mindata, min(order_show) minorder
    FROM
        ITEM_RELATION_TOPSELLER
        where item_code = ItemCode
    GROUP BY item_code , cat_ID_FOR) ToKeep ON ToKeep.ITEM_CODE = ITEM_RELATION_TOPSELLER.ITEM_CODE
        AND ToKeep.cat_ID_FOR = ITEM_RELATION_TOPSELLER.cat_ID_FOR
        AND ToKeep.mindata = ITEM_RELATION_TOPSELLER.Data_Source
        and ToKeep.minorder = ITEM_RELATION_TOPSELLER.Order_Show
WHERE
    ITEM_RELATION_TOPSELLER.ITEM_CODE = ItemCode
ORDER BY data_source, Order_Show
LIMIT 5; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetItemRelatedSellerReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `GetItemRelatedSellerReport`(ItemCode varchar(50))
BEGIN

SELECT 
    ITEM_RELATION_TOPSELLER.*
FROM
       ITEM_RELATION_TOPSELLER
        INNER JOIN
    (SELECT 
        item_code, cat_ID_FOR, MIN(data_source)  mindata, min(order_show) minorder
    FROM
        ITEM_RELATION_TOPSELLER
        where item_code = ItemCode
    GROUP BY item_code , cat_ID_FOR) ToKeep ON ToKeep.ITEM_CODE = ITEM_RELATION_TOPSELLER.ITEM_CODE
        AND ToKeep.cat_ID_FOR = ITEM_RELATION_TOPSELLER.cat_ID_FOR
        AND ToKeep.mindata = ITEM_RELATION_TOPSELLER.Data_Source
        and ToKeep.minorder = ITEM_RELATION_TOPSELLER.Order_Show
WHERE
    ITEM_RELATION_TOPSELLER.ITEM_CODE = ItemCode
ORDER BY data_source, Order_Show
LIMIT 5; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateItemBestSeller` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `UpdateItemBestSeller`(DataTimeAgo int)
BEGIN

SET SQL_SAFE_UPDATES = 0; 

DELETE FROM ITEM_BEST_SELLER 
WHERE ItemMasterID IN (SELECT ID FROM ITEM_MASTER WHERE ITEM_INACTIVE = 'Enable');

INSERT INTO ITEM_BEST_SELLER (ItemMasterID)
SELECT ID FROM ITEM_MASTER 
WHERE ITEM_INACTIVE = 'Disable' AND ID NOT IN (SELECT ItemMasterID FROM ITEM_BEST_SELLER);

DROP TABLE IF EXISTS MyTmpRank;
DROP TABLE IF EXISTS PrimaryData;

CREATE TEMPORARY TABLE PrimaryData (
    order_id int, 
    item_track_no varchar(20), 
    product_code varchar(30),
    print_methods varchar(50),
    rotate_dept varchar(50),
    order_date datetime);
    
INSERT INTO PrimaryData
SELECT  d.order_id, item_track_no, product_code, print_methods, rotate_dept, order_date
FROM discm_discountmugs.dm_order_master m
INNER JOIN discm_discountmugs.dm_order_details d 
      ON m.order_id = d.order_id
WHERE datediff(now(), m.order_date) <= DataTimeAgo
      AND rotate_dept <> 'cancelled'
      AND print_methods NOT IN ( 'blank', 'Samples' )
      AND redo_ref = 0 ;

CREATE TEMPORARY TABLE MyTmpRank (
    ItemID INT,
    cat INT,
    subcat INT,
    Sales INT,
    globalrank INT,
    catrank INT,
    subcatrank INT
);

INSERT INTO  MyTmpRank (ItemID , cat , subcat ,  Sales )
SELECT im.Id, cat_id, Sub_cat_id,   count(d.order_id) OrderCountByItem
FROM ITEM_MASTER im
    INNER JOIN  PrimaryData d
        ON d.product_code = im.ITEM_CODE
WHERE im.ITEM_INACTIVE = 'Disable'
GROUP BY item_code
ORDER BY OrderCountByItem desc ;

SET @row_number = 0;
UPDATE MyTmpRank 
SET 
    globalrank = (@row_number:=@row_number + 1)
ORDER BY sales DESC;

UPDATE ITEM_BEST_SELLER,
    MyTmpRank 
SET 
    GlobalRankByOrderSales = globalrank
WHERE
 ITEM_BEST_SELLER.ItemMasterID = MyTmpRank.itemid;


UPDATE ITEM_BEST_SELLER a,
    (SELECT 
        gl.itemid,
            (@rn:=IF(@lastCatId = cat, @rn + 1, IF(@lastCatId:=cat, 1, 1))) AS cat_rank
    FROM
        (SELECT 
        gl.*
    FROM
        MyTmpRank gl
    ORDER BY gl.cat , gl.sales DESC) gl
    CROSS JOIN (SELECT @rn:=0, @lastCatId:=0) params) querycatrank 
SET 
    a.CategoryRankByOrderSales = querycatrank.cat_rank
WHERE
    querycatrank.itemid = a.ItemMasterID;
    
UPDATE ITEM_BEST_SELLER a,
    (SELECT 
        tmp.itemid,
            (@rn:=IF(@lastsubcatId = subcat, @rn + 1, IF(@lastsubcatId:=subcat, 1, 1))) AS subcat_rank
    FROM
        (SELECT 
        tmp.*
    FROM
        MyTmpRank tmp
    ORDER BY tmp.subcat , tmp.sales DESC) tmp
    CROSS JOIN (SELECT @rn:=0, @lastsubcatId:=0) params) querysubcatrank 
SET 
    a.SubCategoryRankByOrderSales = querysubcatrank.subcat_rank
WHERE
    querysubcatrank.itemid = a.ItemMasterID;

SET SQL_SAFE_UPDATES=1;
   
DROP TABLE IF EXISTS MyTmpRank;
DROP TABLE IF EXISTS PrimaryData;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateItemRelatedSeller` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `UpdateItemRelatedSeller`(DataTimeAgo int)
BEGIN

DROP TABLE IF EXISTS PrimaryData;
DROP TABLE IF EXISTS MyTmpRank;

CREATE TEMPORARY TABLE PrimaryData (
     Item_Code varchar(20),     
     order_id int);
     
CREATE TEMPORARY TABLE MyTmpRank (
  Item_Code varchar(20),
  Item_Related varchar(20),
  Ranking int);

INSERT INTO PrimaryData
SELECT DISTINCT  d.product_code ITEM_CODE, d.order_id
FROM ITEM_MASTER im 
    INNER JOIN discm_discountmugs.dm_order_details d
        ON d.product_code = im.ITEM_CODE
    INNER JOIN discm_discountmugs.dm_order_master m
        ON m.order_id = d.order_id
    INNER JOIN
    (
        SELECT od.order_id,
               COUNT(distinct product_code) qty
        FROM discm_discountmugs.dm_order_details od
        INNER JOIN discm_discountmugs.dm_order_master ms
            ON ms.order_id = od.order_id
		WHERE datediff(now(), ms.order_date) <= DataTimeAgo
        GROUP BY order_id
    ) more
        ON more.order_id = d.order_id
WHERE rotate_dept <> 'cancelled'
      AND im.ITEM_INACTIVE = 'Disable'
      AND datediff(now(), m.order_date) <= DataTimeAgo
      AND more.qty > 1
      AND print_methods NOT IN ( 'blank', 'Samples' )
      AND redo_ref = 0 ;

INSERT INTO MyTmpRank
SELECT 
    gl.Item_code,
    product_code,
    (@rn:=IF(@lastItemId = ITEM_CODE,
        @rn + 1,
        IF(@lastItemId:=ITEM_CODE, 1, 1))) AS Item_rank
FROM
    (SELECT 
        ITEM_CODE, d.product_code, COUNT(d.order_id) orders
    FROM
        discm_discountmugs.dm_order_details d
    INNER JOIN discm_discountmugs.dm_order_master m ON m.order_id = d.order_id
    INNER JOIN (SELECT 
        order_id, COUNT(distinct product_code) qty
    FROM
        discm_discountmugs.dm_order_details
    GROUP BY order_id) more ON more.order_id = d.order_id
    INNER JOIN PrimaryData ON PrimaryData.order_id = more.order_id
    WHERE
        rotate_dept <> 'cancelled'
            AND DATEDIFF(NOW(), m.order_date) <= DataTimeAgo
            AND more.qty > 1
            AND print_methods NOT IN ('blank' , 'Samples')
            AND redo_ref = 0
            AND d.order_id IN (SELECT 
                order_id
            FROM
                discm_discountmugs.dm_order_details
            WHERE
                product_code = PrimaryData.ITEM_CODE)
    GROUP BY ITEM_CODE , d.product_code
    ORDER BY ITEM_CODE , orders DESC) gl
        CROSS JOIN
    (SELECT @rn:=0, @lastItemId:=0) params;
    
    
SET SQL_SAFE_UPDATES=0;
DELETE FROM ITEM_RELATION_SELLER;
INSERT INTO ITEM_RELATION_SELLER
SELECT  m.id, 
        r.id,
        Ranking
FROM MyTmpRank mr 
INNER JOIN ITEM_MASTER m ON m.Item_Code = mr.Item_Code 
INNER JOIN ITEM_MASTER r ON r.Item_Code = mr.Item_Related
WHERE  Ranking < 7;

SET SQL_SAFE_UPDATES=1;
    
DROP TABLE IF EXISTS PrimaryData;
DROP TABLE IF EXISTS MyTmpRank;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateItemRelatedTopSeller` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mortiz`@`%` PROCEDURE `UpdateItemRelatedTopSeller`()
BEGIN

SET SQL_SAFE_UPDATES=0;

DELETE FROM ITEM_RELATION_TOPSELLER;

INSERT INTO ITEM_RELATION_TOPSELLER
SELECT  im.Item_Code,  
        im.CAT_ID,  
		bs.Override ,
		bs.Cat_ID ,
		1,          /**OVERRIDE RANK SOURCE**/
        0           /**HIGH PRIORITY**/
FROM ITEM_MASTER im CROSS JOIN
(SELECT ITEM_CODE Override , ITEM_MASTER.cat_ID 
 FROM ITEM_MASTER INNER JOIN 
      ITEM_BEST_SELLER  ON ItemMasterID = id
WHERE  GlobalOverwriteRank <> 0
        AND GlobalExclusion = 0
        AND ITEM_INACTIVE = 'Disable'
        )bs 
WHERE im.CAT_ID <> bs.Cat_ID;
        
INSERT INTO ITEM_RELATION_TOPSELLER   
 SELECT 
    im.Item_Code,
    im.CAT_ID,
    bs.ItemRelated,
    bs.Cat_ID,
    2,                     /**RELATED ITEMS SOLD RANK SOURCE**/
    RankByItem /**FIRST PRIORITY**/
FROM
    ITEM_MASTER im
        INNER  JOIN
    (SELECT 
            minorder.ItemMasterID,
            ITEM_MASTER.ITEM_CODE ItemRelated,
            ITEM_MASTER.cat_ID,
            RankByItem
    FROM
        (SELECT 
        ItemMasterID, cat_id, MIN(RankByItem) minrank
    FROM
        ITEM_RELATION_SELLER
    INNER JOIN ITEM_MASTER ON id = ItemMasterIDRelated       
        GROUP BY ItemMasterID, CAT_ID) minorder
    
    INNER JOIN ITEM_RELATION_SELLER ON 
    minorder.ItemMasterID = ITEM_RELATION_SELLER.ItemMasterID and 
    minorder.minrank = ITEM_RELATION_SELLER.RankByItem
    INNER JOIN ITEM_MASTER ON ID = ItemMasterIDRelated
    INNER JOIN ITEM_BEST_SELLER  ON ITEM_BEST_SELLER.ItemMasterID = ItemMasterIDRelated
    WHERE  ITEM_INACTIVE = 'Disable'  AND GlobalExclusion = 0
   ) bs on im.ID = ItemMasterID
WHERE
    im.CAT_ID <> bs.Cat_ID;
    
INSERT INTO ITEM_RELATION_TOPSELLER   
 SELECT 
    im.Item_Code,
    im.CAT_ID,
    bs.BestSeller,
    bs.Cat_ID,
    3,                     /**BEST SELLER RANK SOURCE**/
    GlobalRankByOrderSales /**SECOND PRIORITY**/
FROM
    ITEM_MASTER im
        INNER JOIN
    (SELECT 
        ITEM_CODE BestSeller,
            ITEM_MASTER.cat_ID,
            GlobalRankByOrderSales
    FROM
        (SELECT 
        cat_id, MIN(GlobalRankByOrderSales) minrank
    FROM
        ITEM_BEST_SELLER
    INNER JOIN ITEM_MASTER ON id = ItemMasterID
    WHERE
        GlobalRankByOrderSales <> 0       
    GROUP BY CAT_ID) minorder
    INNER JOIN ITEM_BEST_SELLER ON minorder.minrank = ITEM_BEST_SELLER.GlobalRankByOrderSales
    INNER JOIN ITEM_MASTER ON ID = ItemMasterID
    WHERE
        ITEM_INACTIVE = 'Disable' AND GlobalExclusion = 0
    ) bs
WHERE
    im.CAT_ID <> bs.Cat_ID;
    
SET SQL_SAFE_UPDATES=1;    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:43:36
