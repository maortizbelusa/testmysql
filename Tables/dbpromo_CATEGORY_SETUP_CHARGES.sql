-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CATEGORY_SETUP_CHARGES`
--

DROP TABLE IF EXISTS `CATEGORY_SETUP_CHARGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CATEGORY_SETUP_CHARGES` (
  `SETUP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_ID` int(11) NOT NULL,
  `CMYK_SETUP` double NOT NULL DEFAULT '0',
  `LASER_ENGRAVED_SETUP` double NOT NULL DEFAULT '0',
  `EMBROIDERY_SETUP` double NOT NULL DEFAULT '0',
  `EMBROIDERED_PATCHES_SETUP` double NOT NULL,
  `COLOR_TRANSFER_SETUP` double NOT NULL DEFAULT '0',
  `FULL_COLOR_SETUP` double NOT NULL DEFAULT '0',
  `DEBOSSING_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_1_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_2_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_3_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_4_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_5_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_6_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_7_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_COLOR_8_SETUP` double NOT NULL DEFAULT '0',
  `IMPRINT_LIMIT` int(11) DEFAULT NULL,
  `EMBROIDERY_IMPRINT_LIMIT` int(11) NOT NULL,
  `EMBROIDERED_PATCHES_IMPRINT_LIMIT` int(11) NOT NULL,
  `ADDL_IMPRINT_COST` double DEFAULT NULL,
  `MONOGRAMMED_ENGRAVED_SETUP` double NOT NULL,
  `3D_EMBROIDERY_SETUP` double NOT NULL,
  `3D_EMBROIDERY_IMPRINT_LIMIT` int(11) NOT NULL,
  PRIMARY KEY (`SETUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:25:50
