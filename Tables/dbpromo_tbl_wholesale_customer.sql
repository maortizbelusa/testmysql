-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_wholesale_customer`
--

DROP TABLE IF EXISTS `tbl_wholesale_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_wholesale_customer` (
  `who_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ip_address` varchar(20) NOT NULL DEFAULT '',
  `add_date` varchar(100) NOT NULL DEFAULT '',
  `billing_company` varchar(100) NOT NULL DEFAULT '',
  `billing_name` varchar(100) NOT NULL DEFAULT '',
  `billing_address1` varchar(200) NOT NULL DEFAULT '',
  `billing_address2` varchar(200) NOT NULL DEFAULT '',
  `billing_city` varchar(100) NOT NULL DEFAULT '',
  `billing_state` varchar(100) NOT NULL DEFAULT '',
  `billing_zip` varchar(100) NOT NULL DEFAULT '',
  `billing_country` varchar(100) NOT NULL DEFAULT '',
  `billing_email` varchar(100) NOT NULL DEFAULT '',
  `billing_phone` varchar(100) NOT NULL DEFAULT '',
  `shipping_company` varchar(100) NOT NULL DEFAULT '',
  `shipping_name` varchar(100) NOT NULL DEFAULT '',
  `shipping_address1` varchar(100) NOT NULL DEFAULT '',
  `shipping_address2` varchar(100) NOT NULL DEFAULT '',
  `shipping_city` varchar(100) NOT NULL DEFAULT '',
  `shipping_state` varchar(100) NOT NULL DEFAULT '',
  `shipping_zip` varchar(100) NOT NULL DEFAULT '',
  `shipping_country` varchar(100) NOT NULL DEFAULT '',
  `shipping_phone` varchar(100) NOT NULL DEFAULT '',
  `set_value` varchar(100) NOT NULL DEFAULT '',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'N',
  `ponum` varchar(50) NOT NULL DEFAULT '',
  `margin_per1` double DEFAULT NULL,
  `margin_val1` double DEFAULT NULL,
  `margin_per2` double DEFAULT NULL,
  `margin_val2` double DEFAULT NULL,
  PRIMARY KEY (`who_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:19:18
