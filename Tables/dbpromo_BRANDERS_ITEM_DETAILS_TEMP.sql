-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BRANDERS_ITEM_DETAILS_TEMP`
--

DROP TABLE IF EXISTS `BRANDERS_ITEM_DETAILS_TEMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BRANDERS_ITEM_DETAILS_TEMP` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_CODE` varchar(100) NOT NULL,
  `BEL_ITEM_CODE` varchar(50) DEFAULT NULL,
  `ITEM_COLOR` varchar(100) NOT NULL,
  `BEL_ITEM_COLOR` varchar(100) DEFAULT NULL,
  `ITEM_COST` varchar(100) DEFAULT NULL,
  `ITEM_MIN_QTY` varchar(255) DEFAULT NULL,
  `ITEM_PALLET` varchar(255) DEFAULT NULL,
  `ITEM_PALLET_WT` varchar(255) DEFAULT NULL,
  `IMAGE` varchar(255) DEFAULT NULL,
  `ZOOM_IMAGE` varchar(255) DEFAULT NULL,
  `THUMB_IMAGE` varchar(255) DEFAULT NULL,
  `ITEM_COLOR_PALLET` varchar(255) DEFAULT NULL,
  `DELAB_THUMB_IMAGE` varchar(255) DEFAULT NULL,
  `DELAB_BACK_IMAGE` varchar(255) DEFAULT NULL,
  `DELAB_FRONT_IMAGE` varchar(255) DEFAULT NULL,
  `IS_BLUE_INK` enum('Yes','No') DEFAULT 'No',
  `ITEM_PENARC` enum('A','T','N') DEFAULT 'N',
  `ITEM_DISCONTINUED` enum('Y','N') DEFAULT 'N',
  `STOCK_NONSTOCK` enum('stock','nonstock') DEFAULT 'stock',
  `DELAB_IMAGE_STATUS` enum('Enable','Disable') DEFAULT 'Disable',
  `ITEM_COLOR_INACTIVE` enum('Enable','Disable') DEFAULT 'Disable',
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `PSD` varchar(100) DEFAULT NULL,
  `PSDBLANK` varchar(100) DEFAULT NULL,
  `ITEM_COLOR_CLEARANCE` enum('Enable','Disable') DEFAULT 'Disable',
  `RAW_MATERIAL` enum('0','1') DEFAULT '1',
  `MIN_REORDER_QTY` int(11) NOT NULL DEFAULT '0',
  `REORDER_QTY` int(11) DEFAULT '0',
  `SPRAY_ITEM` enum('0','1') DEFAULT '0',
  `3DAY_PRODUCTION` enum('0','1') DEFAULT '0',
  `24HR_PRODUCTION` enum('0','1') DEFAULT '0',
  `IS_DROPSHIP` enum('0','1') NOT NULL DEFAULT '0',
  `SPRAY_OVERRIDE` enum('0','1') NOT NULL DEFAULT '0',
  `Bel_SKU` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_CODE_INDEX` (`ITEM_CODE`),
  KEY `BEL_ITEM_CODE_IDX` (`BEL_ITEM_CODE`,`BEL_ITEM_COLOR`)
) ENGINE=InnoDB AUTO_INCREMENT=80936 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:14:22
