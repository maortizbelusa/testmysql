-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `runner_ticket_support`
--

DROP TABLE IF EXISTS `runner_ticket_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `runner_ticket_support` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_code` int(11) DEFAULT NULL,
  `priority` enum('low','high','medium') DEFAULT 'medium',
  `order_no` varchar(15) NOT NULL,
  `ship_date` date NOT NULL,
  `in_hands_date` date NOT NULL,
  `ship_method` varchar(50) NOT NULL,
  `comments` text,
  `assigned_to` varchar(50) DEFAULT NULL,
  `new_reply` enum('0','1') DEFAULT '0',
  `is_checkmark` enum('0','1') DEFAULT '0',
  `is_emergency` enum('0','1') DEFAULT '0',
  `is_production_sample` enum('0','1') DEFAULT '0',
  `is_checked` enum('0','1') DEFAULT '0',
  `isactive` enum('0','1') DEFAULT '1',
  `submit_by` varchar(50) DEFAULT NULL,
  `submit_date` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_cared` enum('0','1') DEFAULT '0',
  `care_time` datetime DEFAULT NULL,
  `care_by` varchar(100) DEFAULT NULL,
  `closed_date` datetime DEFAULT NULL,
  `is_packing_instructions` enum('0','1') DEFAULT '0',
  `packing_instructions_changed` enum('0','1') DEFAULT '0',
  `packing_instructions_date` datetime DEFAULT NULL,
  `packing_instructions_by` varchar(50) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `color_code` varchar(20) NOT NULL,
  `created_from` varchar(50) NOT NULL,
  `posted_print_method` varchar(100) NOT NULL,
  `added_to_followup_page` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`ticket_id`),
  KEY `ticket_search_idx` (`order_no`,`assigned_to`,`submit_date`,`ticket_code`,`submit_by`)
) ENGINE=InnoDB AUTO_INCREMENT=161526 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:24:06
