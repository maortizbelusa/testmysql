-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_price_mastero_newprice_08012015`
--

DROP TABLE IF EXISTS `tbl_price_mastero_newprice_08012015`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_price_mastero_newprice_08012015` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `pd_code` varchar(100) NOT NULL DEFAULT '',
  `color` char(1) DEFAULT '',
  `avail` enum('Y','N') DEFAULT 'N',
  `pd_12` double NOT NULL DEFAULT '0',
  `pd_36` double NOT NULL DEFAULT '0',
  `pd_72` double NOT NULL DEFAULT '0',
  `pd_144` double NOT NULL DEFAULT '0',
  `pd_288` double NOT NULL DEFAULT '0',
  `pd_576` double NOT NULL DEFAULT '0',
  `pd_1008` double NOT NULL DEFAULT '0',
  `pd_2016` double NOT NULL DEFAULT '0',
  `pd_7500` double NOT NULL DEFAULT '0',
  `pd_10000` double NOT NULL DEFAULT '0',
  `pd_blankmark` double NOT NULL DEFAULT '0',
  `pd_blankmarkw` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`p_id`),
  KEY `pd_code_idx` (`pd_code`),
  KEY `color_idx` (`color`),
  KEY `avail_idx` (`avail`)
) ENGINE=InnoDB AUTO_INCREMENT=7938 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:26:50
