-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DESIGN_ARCHIVE_META_DETAILS`
--

DROP TABLE IF EXISTS `DESIGN_ARCHIVE_META_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DESIGN_ARCHIVE_META_DETAILS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLIP_CAT_ID` int(11) NOT NULL,
  `PAGE_TITLE` varchar(256) NOT NULL,
  `PAGE_KEY` varchar(256) NOT NULL,
  `H1_CONTENT` text NOT NULL,
  `H2_CONTENT` text NOT NULL,
  `H3_CONTENT` text NOT NULL,
  `META_TITLE` text NOT NULL,
  `META_DESCRIPTION` text NOT NULL,
  `META_KEYWORDS` text NOT NULL,
  `HEADER_CONTENT` text NOT NULL,
  `FOOTER_CONTENT` text NOT NULL,
  `BANNER_CONTENT` text NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `CREATED_BY` varchar(256) NOT NULL,
  `META_INACTIVE` enum('Enable','Disable') DEFAULT 'Enable',
  `SECTION` varchar(32) NOT NULL,
  `CLIP_PARENT` int(11) NOT NULL,
  `INTERNAL_LINKING` text NOT NULL,
  `INACTIVE` enum('Enable','Disable') NOT NULL DEFAULT 'Disable',
  `REDIRECT_301` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=459 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:27:12
