-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_markup`
--

DROP TABLE IF EXISTS `product_markup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_markup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_sku` varchar(200) NOT NULL,
  `print_method` varchar(50) NOT NULL,
  `production_method` varchar(50) NOT NULL,
  `markup_1` double DEFAULT '0',
  `markup_2` double DEFAULT '0',
  `markup_3` double DEFAULT '0',
  `markup_4` double DEFAULT '0',
  `markup_5` double DEFAULT '0',
  `markup_6` double DEFAULT '0',
  `markup_7` double DEFAULT '0',
  `markup_8` double DEFAULT '0',
  `markup_9` double DEFAULT '0',
  `markup_10` double DEFAULT '0',
  `markup_11` double DEFAULT '0',
  `markup_12` double DEFAULT '0',
  `markup_13` double DEFAULT '0',
  `markup_14` double DEFAULT '0',
  `markup_15` double DEFAULT '0',
  `markup_16` double DEFAULT '0',
  `markup_17` double DEFAULT '0',
  `markup_18` double DEFAULT '0',
  `markup_19` double DEFAULT '0',
  `markup_20` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1153 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:25:55
