-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_shipping_price`
--

DROP TABLE IF EXISTS `tbl_shipping_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_shipping_price` (
  `shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `subset_id` int(11) DEFAULT NULL,
  `categories` varchar(200) NOT NULL,
  `sub_categories` varchar(200) NOT NULL,
  `l_shipping_price_12_35` double NOT NULL DEFAULT '0',
  `h_shipping_price_12_35` double NOT NULL DEFAULT '0',
  `l_shipping_price_36_71` double NOT NULL DEFAULT '0',
  `h_shipping_price_36_71` double NOT NULL DEFAULT '0',
  `l_shipping_price_72_143` double NOT NULL DEFAULT '0',
  `h_shipping_price_72_143` double NOT NULL DEFAULT '0',
  `l_shipping_price_144_287` double NOT NULL DEFAULT '0',
  `h_shipping_price_144_287` double NOT NULL DEFAULT '0',
  `l_shipping_price_288_575` double NOT NULL DEFAULT '0',
  `h_shipping_price_288_575` double NOT NULL DEFAULT '0',
  `l_shipping_price_576_1007` double NOT NULL DEFAULT '0',
  `h_shipping_price_576_1007` double NOT NULL DEFAULT '0',
  `l_shipping_price_1008_2015` double NOT NULL DEFAULT '0',
  `h_shipping_price_1008_2015` double NOT NULL DEFAULT '0',
  `l_shipping_price_2016` double NOT NULL DEFAULT '0',
  `h_shipping_price_2016` double NOT NULL DEFAULT '0',
  `l_shipping_price_7500_9999` double NOT NULL DEFAULT '0',
  `h_shipping_price_7500_9999` double NOT NULL DEFAULT '0',
  `l_shipping_price_10000` double NOT NULL DEFAULT '0',
  `h_shipping_price_10000` double NOT NULL DEFAULT '0',
  `3_day_shipment_price` double NOT NULL DEFAULT '0',
  `2_day_shipment_price` double NOT NULL DEFAULT '0',
  `overnight_shipment_price` double NOT NULL DEFAULT '0',
  `saturday_shipment_price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:37:41
