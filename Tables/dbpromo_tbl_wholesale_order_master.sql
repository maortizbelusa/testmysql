-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_wholesale_order_master`
--

DROP TABLE IF EXISTS `tbl_wholesale_order_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_wholesale_order_master` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `who_id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL DEFAULT '',
  `order_date` varchar(100) NOT NULL DEFAULT '',
  `ponum` varchar(50) NOT NULL DEFAULT '',
  `billing_company` varchar(200) NOT NULL DEFAULT '',
  `billing_name` varchar(200) NOT NULL DEFAULT '',
  `billing_address1` varchar(200) NOT NULL DEFAULT '',
  `billing_address2` varchar(200) NOT NULL DEFAULT '',
  `billing_city` varchar(200) NOT NULL DEFAULT '',
  `billing_state` varchar(200) NOT NULL DEFAULT '',
  `billing_zip` varchar(200) NOT NULL DEFAULT '',
  `billing_county` varchar(200) NOT NULL DEFAULT '',
  `billing_email` varchar(200) NOT NULL DEFAULT '',
  `billing_phone` varchar(200) NOT NULL DEFAULT '',
  `billing_po` varchar(200) NOT NULL DEFAULT '',
  `shipping_name` varchar(200) NOT NULL DEFAULT '',
  `shipping_company` varchar(200) NOT NULL DEFAULT '',
  `shipping_address1` varchar(200) NOT NULL DEFAULT '',
  `shipping_address2` varchar(200) NOT NULL DEFAULT '',
  `shipping_city` varchar(200) NOT NULL DEFAULT '',
  `shipping_state` varchar(200) NOT NULL DEFAULT '',
  `shipping_zip` varchar(200) NOT NULL DEFAULT '',
  `shipping_country` varchar(200) NOT NULL DEFAULT '',
  `grand_order_total` varchar(200) NOT NULL DEFAULT '',
  `payment_status` varchar(200) NOT NULL DEFAULT '',
  `notes` varchar(200) NOT NULL DEFAULT '',
  `hmail` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:11:44
