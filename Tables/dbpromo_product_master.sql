-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_master`
--

DROP TABLE IF EXISTS `product_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_master` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(30) NOT NULL,
  `product_color` varchar(50) NOT NULL,
  `price_12_35` double NOT NULL DEFAULT '0',
  `price_36_71` double NOT NULL DEFAULT '0',
  `price_72_143` double NOT NULL DEFAULT '0',
  `price_144_287` double NOT NULL DEFAULT '0',
  `price_288_575` double NOT NULL DEFAULT '0',
  `price_576_1007` double NOT NULL DEFAULT '0',
  `price_1008_2015` double NOT NULL DEFAULT '0',
  `price_2016` double NOT NULL DEFAULT '0',
  `price_7500_9999` double NOT NULL DEFAULT '0',
  `price_10000` double NOT NULL DEFAULT '0',
  `price_20000` double NOT NULL DEFAULT '0',
  `price_30000` double NOT NULL DEFAULT '0',
  `screen_charge` double NOT NULL DEFAULT '39',
  `product_image` varchar(255) DEFAULT NULL,
  `product_lint` varchar(255) DEFAULT NULL,
  `colorProcess` int(11) DEFAULT NULL,
  `magnet` int(11) DEFAULT '0',
  `giftboxAvailable` int(11) DEFAULT '1',
  `trim_color` varchar(50) NOT NULL DEFAULT '',
  `barrel_color` varchar(50) NOT NULL DEFAULT '',
  `ink_color` varchar(50) NOT NULL DEFAULT '',
  `grip_color` varchar(50) NOT NULL DEFAULT '',
  `min_quantity` int(10) NOT NULL DEFAULT '0',
  `is_3day` enum('1','0') NOT NULL DEFAULT '1',
  `is_24hours` enum('1','0') NOT NULL DEFAULT '1',
  `is_clip_printing` enum('1','0') NOT NULL DEFAULT '0',
  `is_promotional` enum('1','0') NOT NULL DEFAULT '0',
  `is_medium_point` enum('1','0') NOT NULL DEFAULT '0',
  `is_fine_point` enum('1','0') NOT NULL DEFAULT '0',
  `assorted_color` int(10) NOT NULL DEFAULT '0',
  `piece_per_box` int(10) NOT NULL DEFAULT '0',
  `is_libbey` enum('1','0') NOT NULL DEFAULT '0',
  `is_free_overnight_shipping` enum('0','1','2') NOT NULL DEFAULT '0',
  `disable3day_price` enum('1','0') NOT NULL DEFAULT '0',
  `namemug_price` double NOT NULL DEFAULT '5.99',
  `disable` enum('0','1') DEFAULT '0',
  `title_description` text,
  `gift_style` varchar(10) DEFAULT NULL,
  `gift_available` int(1) DEFAULT '0',
  `print_on` text,
  `saucer_image` varchar(200) DEFAULT NULL,
  `shipping_discount_qty_range` varchar(250) DEFAULT NULL,
  `is_handle` enum('Yes','No') DEFAULT 'No',
  `sub_item` varchar(50) NOT NULL,
  `size` varchar(255) DEFAULT NULL,
  `imprint_type` varchar(255) NOT NULL DEFAULT 'Custom',
  `is_3day_free` enum('0','1') NOT NULL DEFAULT '0',
  `is_24hours_free` enum('0','1') NOT NULL DEFAULT '0',
  `is_unconstructed` enum('0','1') NOT NULL DEFAULT '0',
  `is_color_transfer` enum('0','1') NOT NULL DEFAULT '0',
  `is_free_ship` enum('0','1') NOT NULL DEFAULT '0',
  `is_2nd_side` enum('1','0') NOT NULL DEFAULT '0',
  `is_spray_print` enum('Yes','No') NOT NULL DEFAULT 'No',
  `stock_nonstock` enum('stock','nonstock') NOT NULL DEFAULT 'stock',
  `is_blue_ink` enum('Yes','No') NOT NULL DEFAULT 'No',
  `cost_price` double NOT NULL,
  `oos_notify` enum('0','1') NOT NULL DEFAULT '0',
  `drop_ship` enum('0','1') DEFAULT '0',
  `inactive` enum('0','1') NOT NULL DEFAULT '0',
  `vendor_id` char(65) NOT NULL,
  `open_box` enum('0','1') NOT NULL,
  `price_slab_13` double NOT NULL,
  `price_slab_14` double NOT NULL,
  `price_slab_15` double NOT NULL,
  `price_slab_16` double NOT NULL,
  `price_slab_17` double NOT NULL,
  `price_slab_18` double NOT NULL,
  `price_slab_19` double NOT NULL,
  `price_slab_20` double NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_code` (`product_code`),
  KEY `product_color_idx` (`product_color`)
) ENGINE=InnoDB AUTO_INCREMENT=70179 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:18:07
