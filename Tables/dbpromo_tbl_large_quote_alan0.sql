-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_large_quote_alan`
--

DROP TABLE IF EXISTS `tbl_large_quote_alan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_large_quote_alan` (
  `q_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_no` varchar(20) NOT NULL DEFAULT '',
  `customer_name` varchar(200) NOT NULL DEFAULT '',
  `company_name` varchar(200) NOT NULL DEFAULT '',
  `phone_no` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `item_code` varchar(200) NOT NULL DEFAULT '',
  `item_color` varchar(200) NOT NULL DEFAULT '',
  `qty` varchar(200) NOT NULL DEFAULT '',
  `netcost` varchar(200) NOT NULL DEFAULT '',
  `qty1` varchar(200) NOT NULL DEFAULT '',
  `netcost1` varchar(200) NOT NULL DEFAULT '',
  `qty2` varchar(200) NOT NULL DEFAULT '',
  `netcost2` varchar(200) NOT NULL DEFAULT '',
  `qty3` varchar(200) NOT NULL DEFAULT '',
  `netcost3` varchar(200) NOT NULL DEFAULT '',
  `qty4` varchar(200) NOT NULL DEFAULT '',
  `netcost4` varchar(200) NOT NULL DEFAULT '',
  `include` varchar(200) NOT NULL DEFAULT '',
  `includeot` varchar(200) NOT NULL DEFAULT '',
  `setup` varchar(200) NOT NULL DEFAULT '',
  `setupot` varchar(200) NOT NULL DEFAULT '',
  `presample` varchar(200) NOT NULL DEFAULT '',
  `presampleot` varchar(200) NOT NULL DEFAULT '',
  `pot` varchar(200) NOT NULL DEFAULT '',
  `potot` varchar(200) NOT NULL DEFAULT '',
  `fob` varchar(200) NOT NULL DEFAULT '',
  `fobot` varchar(200) NOT NULL DEFAULT '',
  `validity` varchar(200) NOT NULL DEFAULT '',
  `validityot` varchar(200) NOT NULL DEFAULT '',
  `inst` text NOT NULL,
  `date` varchar(200) NOT NULL DEFAULT '',
  `admin` text NOT NULL,
  `admindate` varchar(200) NOT NULL DEFAULT '',
  `statusadmin` enum('1','0') NOT NULL DEFAULT '1',
  `sendername` varchar(200) NOT NULL DEFAULT '',
  `data1` varchar(200) NOT NULL DEFAULT '',
  `data2` varchar(200) NOT NULL DEFAULT '',
  `data3` varchar(200) NOT NULL DEFAULT '',
  `data4` varchar(200) NOT NULL DEFAULT '',
  `data4ot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`q_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:36:59
