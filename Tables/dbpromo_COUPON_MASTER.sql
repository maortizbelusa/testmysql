-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 172.31.49.108    Database: dbpromo
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `COUPON_MASTER`
--

DROP TABLE IF EXISTS `COUPON_MASTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COUPON_MASTER` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(20) NOT NULL DEFAULT '',
  `discount_amount` int(4) NOT NULL DEFAULT '0',
  `section` text NOT NULL,
  `expire_date` date NOT NULL DEFAULT '0000-00-00',
  `discount_on` enum('S','T','I') DEFAULT 'T',
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `clearance_item` enum('0','1') DEFAULT '0',
  `coupon_type` enum('redo_coupon','compensation_coupon','regular_coupon','dollar_discount','setup_discount','free_shipping') NOT NULL DEFAULT 'regular_coupon',
  `special_comment` text NOT NULL,
  `blank_skip` enum('0','1') NOT NULL DEFAULT '0',
  `display_copuon_as` varchar(150) NOT NULL,
  `dollar_discount_amount` varchar(150) NOT NULL,
  `dollar_discount_min_qty` varchar(150) NOT NULL,
  `only_daily_deals` enum('0','1') NOT NULL DEFAULT '0',
  `setup_discount_min_amt` float NOT NULL,
  `only_clearance_item` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_id`),
  KEY `IDX_COUPON` (`coupon_code`,`expire_date`,`discount_on`)
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 12:14:57
